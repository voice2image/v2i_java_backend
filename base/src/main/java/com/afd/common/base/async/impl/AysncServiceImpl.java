package com.afd.common.base.async.impl;

import com.afd.common.base.async.AsyncService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.concurrent.*;

/**
 * Created by faisal on 25/1/18.
 */
@Service
@Slf4j
public class AysncServiceImpl implements AsyncService {

        private BlockingQueue<Runnable> workQueue;

        private ExecutorService exec;

        @Value("${core.pool.size.for.queue:10}")
        private int corePoolSize;

        @Value("${core.pool.max.size.for.queue:20}")
        private int maxPoolSize;

        @Value("${async.queue.size:100000}")
        private int queueSize;

        @Value("${async.queue.timeout.in.minutes:10}")
        private int timeout;

        @PostConstruct
        public void inititize() {
            workQueue = new LinkedBlockingQueue<Runnable>(queueSize);
            exec = new ThreadPoolExecutor(corePoolSize, maxPoolSize,timeout, TimeUnit.MINUTES, workQueue);
        }

    @Override
    public boolean doAsync(Runnable r) {
        return doAysnc(2,5*1000,r);
    }

    @Override
    public boolean doAsyncNoWait(Runnable r) {
        return doAysnc(0,0,r);
    }


    public boolean doAysnc(int retry, long timeout, Runnable r) {
            try{
                boolean isQueued = false;
                while(!isQueued){
                    if (workQueue.remainingCapacity() > 0) {
                        exec.execute(r);
                        isQueued = true;
                        break;
                    }
                    if (!isQueued && retry < 1) {
                        log.warn("Async task is waiting for queue allocation.");
                        try {
                            Thread.sleep(timeout);
                            retry++;
                        } catch (InterruptedException e) {
                        }
                    }
                    if (retry == 2) {
                        log.error("Queue full, cannot allocate.");
                    }
                }
                log.info("Async task queued for processing.");
                return isQueued;

            }catch(Exception e){
                log.error("Error occurred in saving async task = " + r,e);
            }
            return false;
        }
}
