package com.afd.common.base.exception;

import lombok.Getter;

/**
 * Created by faisal on 27/1/18.
 */
@Getter
public enum TransportErrorCodes {

    SERIALIZATION_EXCEPTION(101, "Serialization failed"),
    DESERIALIZATION_EXCEPTION(102, "Deserialization failed"),

    IO_EXCEPTION(1001,"Exception in IO in processing request"),
    REQUEST_TYPE_ERROR_EXCEPTION(2001,"Exception in the request type"),
    URL_MISSING(5001,"URL not defined"),
    INPUT_MISSING(5002,"Input missing"),
    UNKOWN_EXCEPTION(10000, "Unkown error");

    private int    code;
    private String description;

    private TransportErrorCodes(int code, String description) {
        this.code = code;
        this.description = description;
    }

}
