package com.afd.common.base.cache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;

/**
 * Created by faisal on 6/1/17.
 */
@Service
public class CacheService {

    @Autowired
    private CacheManager cacheManager;

    public void putToCache(String cache, Object value, Object... keys) {
        cacheManager.getCache(cache).put(getKey(keys),value);
    }

    private String getKey(Object[] keys) {
        String key = "";
        for (int keyIndex = 0; keyIndex < keys.length; keyIndex++) {
            key += keys[keyIndex].toString();
        }
        return key;
    }

    public void removeFromCache(String cache, Object value, Object... keys) {
        cacheManager.getCache(cache).evict(getKey(keys));
    }

    public <T> T getFromCache(String cache, Class<T> clazz, Object... keys) {
        return cacheManager.getCache(cache).get(getKey(keys), clazz);
    }
}
