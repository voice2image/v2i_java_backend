package com.afd.common.base.constants;

/**
 * The supported protocols.
 *
 * Created by jaiprakash on 27/12/16.
 */
public enum Protocol {

    PROTOCOL_JSON("application/json"), PROTOCOL_PROTOSTUFF("application/x-protobuf"), PROTOCOL_XML("application/xml");

    private String applicationType;

    private Protocol(String applicationType) {
        this.applicationType = applicationType;
    }

    public String getApplicationType() {
        return applicationType;
    }
}
