package com.afd.common.base.reqresp.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by jaiprakash on 6/1/17.
 */
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SubResponseDTO implements Serializable {
}
