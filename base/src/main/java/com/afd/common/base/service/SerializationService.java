package com.afd.common.base.service;


import com.afd.common.base.exception.SerializationException;

/**
 * Created by jaiprakash on 4/1/17.
 */
public interface SerializationService {

    String doSerialize(Object object) throws SerializationException;

    public byte[] doSerialize(Class<? extends Object> classType, Object obj) throws SerializationException;

    public Object doDeserialize(byte[] data, Class<? extends Object> classType) throws SerializationException;

    Object doDeSerialize(String json, Class<? extends Object> classType) throws SerializationException;
}
