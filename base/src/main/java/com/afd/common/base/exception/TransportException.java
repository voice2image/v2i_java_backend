package com.afd.common.base.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by faisal on 27/1/18.
 */
@Getter
@Setter
public class TransportException extends Exception {

    private TransportErrorCodes transportErrorCodes;

    public TransportException(Throwable cause, TransportErrorCodes transportErrorCodes) {
        super(cause);
        this.transportErrorCodes = transportErrorCodes;
    }

    public TransportException(String message, TransportErrorCodes transportErrorCodes) {
        super(message);
        this.transportErrorCodes = transportErrorCodes;
    }

    public TransportException(String message, Throwable cause, TransportErrorCodes transportErrorCodes) {
        super(message, cause);
        this.transportErrorCodes = transportErrorCodes;
    }
}
