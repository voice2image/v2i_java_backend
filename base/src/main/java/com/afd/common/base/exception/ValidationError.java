package com.afd.common.base.exception;

import lombok.*;

/**
 * Created by faisal on 25/1/17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ValidationError {
    private int code;
    private String message;
}