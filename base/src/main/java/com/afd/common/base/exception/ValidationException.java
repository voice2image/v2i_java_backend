package com.afd.common.base.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ValidationException extends Exception {

    private List<ValidationError> validationErrorList;

    public ValidationException(ValidationError error) {
        this.validationErrorList = new ArrayList<>();
        validationErrorList.add(error);
    }

    public void addValidationError(ValidationError validationError) {
        if (this.validationErrorList == null) {
            this.validationErrorList = new ArrayList<>();
        }
        this.validationErrorList.add(validationError);
    }

    private static final long serialVersionUID = -2228709637208422355L;
}
