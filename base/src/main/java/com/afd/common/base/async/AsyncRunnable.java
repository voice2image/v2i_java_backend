package com.afd.common.base.async;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by faisal on 25/1/18.
 */
@Slf4j
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AsyncRunnable implements Runnable {

  public Runnable runnable;

  public void run() {
    try {
        runnable.run();
    }catch (Exception e) {
      log.error("Could not complete execution ", e);
    }
  }
}
