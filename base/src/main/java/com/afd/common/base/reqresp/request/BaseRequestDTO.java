package com.afd.common.base.reqresp.request;

import com.afd.common.base.reqresp.BaseDto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * Created by faisal on 25/1/17.
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class BaseRequestDTO implements Serializable, BaseDto {

    private static final long serialVersionUID = -7949306876351634982L;

}
