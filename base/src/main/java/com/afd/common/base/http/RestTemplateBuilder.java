package com.afd.common.base.http;

import com.afd.common.base.constants.Constants;
import com.afd.common.base.service.config.HttpClientConfig;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.config.SocketConfig;
import org.apache.http.impl.client.DefaultConnectionKeepAliveStrategy;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HttpContext;
import org.springframework.http.MediaType;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;


/**
 * For class creates the rest template used for a provided service.
 *
 * Created by jaiprakash on 27/12/16.
 */
@Slf4j
@Builder
public class RestTemplateBuilder {

    public static RestTemplate createNewRestTemplate(HttpClientConfig httpClientConfig) {
        HttpClientBuilder httpClientBuilder = getHttpClientBuilder(httpClientConfig);
        HttpClient httpClient = httpClientBuilder.build();
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(httpClient);
        factory.setConnectTimeout(httpClientConfig.getConnectionTimeout());
        factory.setConnectionRequestTimeout(httpClientConfig.getConnectionRequestTimeout());
        factory.setReadTimeout(httpClientConfig.getReadTimeout());
        RestTemplate restTemplate = new RestTemplate(factory);
        // Add the String message converter
        restTemplate.getMessageConverters().addAll(getMessageConverters());
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter());
        return restTemplate;
    }

    private static List<HttpMessageConverter<?>> getMessageConverters() {
        MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter =
                                                                            new MappingJackson2HttpMessageConverter();
        mappingJackson2HttpMessageConverter.setSupportedMediaTypes(
                Arrays.asList(new MediaType[]{MediaType.ALL}));

        Jaxb2RootElementHttpMessageConverter jaxb2RootElementHttpMessageConverter =
                                                                            new Jaxb2RootElementHttpMessageConverter();
        jaxb2RootElementHttpMessageConverter.setSupportedMediaTypes(
                Arrays.asList(new MediaType[]{MediaType.ALL}));
        ProtostuffHttpMessageConverter protostuffHttpMessageConverter = new ProtostuffHttpMessageConverter();
        List<HttpMessageConverter<?>> converters = new ArrayList<>();
        converters.add(jaxb2RootElementHttpMessageConverter);
        converters.add(mappingJackson2HttpMessageConverter);
        converters.add(protostuffHttpMessageConverter);
        return converters;
    }

    private static HttpClientBuilder getHttpClientBuilder(HttpClientConfig httpClientConfig) {
        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create()
                .setMaxConnTotal(httpClientConfig.getMaxConnections())
                .setMaxConnPerRoute(httpClientConfig.getMaxConnectionsPerRoute())
                .setConnectionTimeToLive(httpClientConfig.getConnectionTimeToLive(), TimeUnit.SECONDS);
        if (httpClientConfig.isEnableKeepAlive()) {
            httpClientBuilder.setKeepAliveStrategy(new DefaultConnectionKeepAliveStrategy() {
                @Override
                public long getKeepAliveDuration(HttpResponse response, HttpContext context) {
                    return httpClientConfig.getKeepAliveDuration();
                }
            });
        }
        if (httpClientConfig.isEnableGzip()) {
            // Add the gzip Accept-Encoding header
            List<Header> requestHeaders = new ArrayList<>(1);
            requestHeaders.add(new BasicHeader(Constants.ACCEPT_ENCODING_HEADER, Constants.GZIP));
            httpClientBuilder.setDefaultHeaders(requestHeaders);
        }
        httpClientBuilder.setDefaultSocketConfig(SocketConfig.custom().setSoTimeout(httpClientConfig.getSocketTimeout()).build());
        return httpClientBuilder;
    }
}
