package com.afd.common.base.service.config;

import com.afd.common.base.utils.StringUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * The HTTP Configurations per service.
 *
 * Created by faisal on 27/1/18.
 */
@Slf4j
@Getter
@Setter
@ToString
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class HttpClientConfig extends DefaultHttpClientConfig {

    private static final String MAX_CONNECTION_PER_ROUTE = "max.connections.per.route";
    private static final String CONNECTION_TIME_TO_LIVE_IN_SECONDS = "connection.time.to.live.in.seconds";
    private static final String KEEP_ALIVE_DURATION = "keep.alive.duration";
    private static final String ENABLE_KEEP_ALIVE = "enable.keep.alive";
    private static final String CONNECT_TIMEOUT = "connection.timeout";
    private static final String CONNECT_REQUEST_TIMEOUT = "connection.request.timeout";
    private static final String READ_TIMEOUT = "read.timeout";
    private static final String SOCKET_TIMEOUT = "socket.timeout";
    private static final String ENABLE_GZIP = "enable.gzip";

    @Autowired
    private Environment environment;

    @Value("${default.max.connections.per.route:50}")
    private int maxConnectionsPerRoute;

    @Value("${default.connection.time.to.live.in.seconds:10}")
    private int connectionTimeToLive;

    @Value("${default.keep.alive.duration:-1}")
    private int keepAliveDuration;

    @Value("${default.enable.keep.alive:true}")
    private boolean enableKeepAlive;

    @Value("${default.connection.timeout:10000}")
    private int connectionTimeout;

    @Value("${default.connection.request.timeout:10000}")
    private int connectionRequestTimeout;

    @Value("${default.read.timeout:10000}")
    private int readTimeout;

    @Value("${default.socket.timeout:10000}")
    private int socketTimeout;

    @Value("${default.enable.gzip:true}")
    private boolean enableGzip;


    public HttpClientConfig updateValuesForInitials(String initials) {
        log.info("updating values with initials {}", getIntProperty(initials + CONNECT_TIMEOUT,
            this.getConnectionTimeout()));
        this.setMaxConnectionsPerRoute(getIntProperty(initials + MAX_CONNECTION_PER_ROUTE,
            this.getMaxConnectionsPerRoute()));
        this.setConnectionTimeToLive(getIntProperty(initials + CONNECTION_TIME_TO_LIVE_IN_SECONDS,
            this.getConnectionTimeToLive()));
        this.setKeepAliveDuration(getIntProperty(initials + KEEP_ALIVE_DURATION,
            this.getKeepAliveDuration()));
        this.setEnableKeepAlive(getBooleanProperty(initials + ENABLE_KEEP_ALIVE,
            this.isEnableKeepAlive()));
        this.setConnectionRequestTimeout(getIntProperty(initials + CONNECT_REQUEST_TIMEOUT,
            this.getConnectionRequestTimeout()));
        this.setConnectionTimeout(getIntProperty(initials + CONNECT_TIMEOUT,
            this.getConnectionTimeout()));
        this.setReadTimeout(getIntProperty(initials + READ_TIMEOUT, this.getReadTimeout()));
        this.setSocketTimeout(getIntProperty(initials + SOCKET_TIMEOUT, getSocketTimeout()));
        this.setEnableGzip(getBooleanProperty(initials + ENABLE_GZIP, this.isEnableGzip()));
        return this;
    }

    private int getIntProperty(String propertyName, int defaultValue) {
        int responseValue = defaultValue;
        if (StringUtils.isNotEmpty(environment.getProperty(propertyName))) {
            try {
                responseValue = Integer.parseInt(environment.getProperty(propertyName));
            } catch (NumberFormatException e) {
                //Do nothing
            }
        }
        return responseValue;
    }

    private String getProperty(String propertyName, String defaultValue) {
        String responseValue = defaultValue;
        if (StringUtils.isNotEmpty(environment.getProperty(propertyName))) {
            responseValue = environment.getProperty(propertyName);
        }
        return responseValue;
    }

    private boolean getBooleanProperty(String propertyName, boolean defaultValue) {
        boolean responseValue = defaultValue;
        if (StringUtils.isNotEmpty(environment.getProperty(propertyName))) {
            responseValue = Boolean.parseBoolean(environment.getProperty(propertyName));
        }
        return responseValue;
    }

}
