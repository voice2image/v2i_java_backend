package com.afd.common.base.reqresp.response;

import lombok.Getter;

@Getter
public enum ResponseCodeEnum {
    SUCCESS(0),
    VALIDATION_ERROR(1),
    FAILURE(2),
    VERSION_OLD(3),
    COMMUNICATION_BREAKDOWN(4);

    private Integer code;

    ResponseCodeEnum(Integer code){
        this.code = code;
    }
}
