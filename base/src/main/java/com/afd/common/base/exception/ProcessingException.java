package com.afd.common.base.exception;

/**
 * Created by faisal on 27/1/18.
 */
public class ProcessingException extends Exception {
    public ProcessingException(String message, Exception exception) {
        super(message,exception);
    }

    public ProcessingException(String message) {
        super(message);
    }
}
