package com.afd.common.base.http;

import io.protostuff.LinkedBuffer;
import io.protostuff.ProtobufIOUtil;
import io.protostuff.Schema;
import io.protostuff.runtime.RuntimeSchema;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
/**
 * Created by jaiprakash on 4/1/17.
 */
public class ProtostuffHttpMessageConverter<Object> extends AbstractHttpMessageConverter<Object> {
    public static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");
    public static final MediaType PROTOSTUFF = new MediaType("application", "protostuff");

    public ProtostuffHttpMessageConverter() {
        super(PROTOSTUFF);
    }

    protected boolean supports(Class<?> clazz) {
        return true;
    }

    protected Object readInternal(Class<? extends Object> clazz, HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
        Schema<?> schema = RuntimeSchema.getSchema(clazz);
        final Object value = (Object) schema.newMessage();

        try (final InputStream stream = inputMessage.getBody())
        {
            ProtobufIOUtil.mergeFrom(stream, value, (Schema<Object>) schema);
            return value;
        }
    }

    protected void writeInternal(Object message, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
        try (final OutputStream stream = outputMessage.getBody()) {
            ProtobufIOUtil.writeTo(stream, message, RuntimeSchema.getSchema((Class<Object>) message.getClass()), LinkedBuffer.allocate());
        }
    }

    @Override
    public boolean canRead(final Class<?> clazz, final MediaType mediaType)
    {
        return canRead(mediaType);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canWrite(final Class<?> clazz, final MediaType mediaType)
    {
        return canWrite(mediaType);
    }

}