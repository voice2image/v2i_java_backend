package com.afd.common.base.service.impl;

import com.afd.common.base.constants.Constants;
import com.afd.common.base.constants.Protocol;
import com.afd.common.base.exception.TransportErrorCodes;
import com.afd.common.base.exception.TransportException;
import com.afd.common.base.http.RestTemplateBuilder;
import com.afd.common.base.reqresp.BaseDto;
import com.afd.common.base.reqresp.request.BaseRequestDTO;
import com.afd.common.base.reqresp.response.BaseResponseDTO;
import com.afd.common.base.service.TransportService;
import com.afd.common.base.service.config.HttpClientConfig;
import com.afd.common.base.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by faisal on 27/1/18.
 */
@Slf4j
@Service
public class TransportServiceImpl implements TransportService {

    @Autowired
    private ApplicationContext applicationContext;

    private static final Map<String, RestTemplate> httpSenderCache = new ConcurrentHashMap<>();

    private static final Map<String,String> serviceToInitialsMap = new HashMap<>();

    @Override
    public void registerService(String serviceUrl, String cacheName) {
        log.info("Registering service : {} with cache name : {}", serviceUrl, cacheName);
        serviceToInitialsMap.put(serviceUrl, cacheName);
    }

    @Override
    public <T extends BaseResponseDTO> T executeGet(String requestPath, Map<String, String> params, Map<String, String> headersMap, Class<T> classType, String serviceUrl)
        throws TransportException {
        validate(requestPath);
        RestTemplate restTemplate = getCachedRestTemplate(serviceUrl);
        HttpHeaders headers = getHttpHeaders(headersMap, Constants.DEFAULT_PROTOCOL);
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(requestPath);
        if (params != null) {
            params.forEach((key,value) -> builder.queryParam(key,value));
        }
        T responseDTO;
        HttpEntity<String> entity = new HttpEntity<>(headers);
        try {
            ResponseEntity<T> responseEntity = restTemplate.exchange(builder.build().encode(Constants.CONTENT_ENCODING_UTF_8).toUri(), HttpMethod.GET,entity, classType);
            responseDTO = getResponse(responseEntity);
        } catch (UnsupportedEncodingException e) {
           throw new TransportException(e, TransportErrorCodes.SERIALIZATION_EXCEPTION);
        } catch (Exception e) {
            throw new TransportException(e, TransportErrorCodes.UNKOWN_EXCEPTION);
        }
        return responseDTO;
    }

    @Override
    public <T> T executeGet(Class<T> responseType, String requestPath, Map<String, String> params, Map<String, String> headersMap, String serviceUrl) throws TransportException {
        validate(requestPath);
        RestTemplate restTemplate = getCachedRestTemplate(serviceUrl);
        HttpHeaders headers = getHttpHeaders(headersMap, Constants.DEFAULT_PROTOCOL);
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(requestPath);
        if (params != null) {
            params.forEach((key,value) -> builder.queryParam(key,value));
        }
        HttpEntity<String> entity = new HttpEntity<>(headers);
        try {
            ResponseEntity<T> responseEntity = restTemplate.exchange(builder.build().encode(Constants.CONTENT_ENCODING_UTF_8).toUri(), HttpMethod.GET,entity, responseType);
            return responseEntity.getBody();
        } catch (UnsupportedEncodingException e) {
           throw new TransportException(e, TransportErrorCodes.SERIALIZATION_EXCEPTION);
        } catch (Exception e) {
            throw new TransportException(e, TransportErrorCodes.UNKOWN_EXCEPTION);
        }
    }

    private HttpHeaders getHttpHeaders(Map<String, String> headersMap, Protocol protocol) {
        HttpHeaders headers = new HttpHeaders();
        if (headersMap != null) {
            headersMap.forEach(headers::add);
        }
        headers.set(HttpHeaders.CONTENT_TYPE, protocol.getApplicationType());
        headers.set(HttpHeaders.ACCEPT, protocol.getApplicationType());
        return headers;
    }

    private void validate(String requestPath) throws TransportException {
        if (StringUtils.isEmpty(requestPath)) {
            throw new TransportException("No defined url was found url " +requestPath, TransportErrorCodes.URL_MISSING);
        }
    }

    @Override
    public <T extends BaseResponseDTO> T executePost(String requestPath, BaseDto request, Map<String, String> headersMap, Class<T> classType, String serviceUrl) throws TransportException {
        return executePost(requestPath,request,headersMap,classType, Constants.DEFAULT_PROTOCOL,serviceUrl);
    }
    
    @Override
    public <T extends BaseResponseDTO> T executePost(String requestPath, BaseRequestDTO request, Map<String, String> headersMap, Class<T> classType, String serviceUrl) throws TransportException {
        return executePost(requestPath,request,headersMap,classType, Constants.DEFAULT_PROTOCOL,serviceUrl);
    }

    @Override
    public <T extends BaseResponseDTO> T executePost(String requestPath, BaseDto request, Map<String, String> headersMap, Class<T> classType, Protocol protocol, String serviceUrl) throws TransportException {
        validatePost(requestPath,request);
        RestTemplate restTemplate = getCachedRestTemplate(serviceUrl);
        HttpHeaders httpHeaders = getHttpHeaders(headersMap, protocol);
        HttpEntity<BaseDto> entity = new HttpEntity<>(request, httpHeaders);
        ResponseEntity<T> responseEntity = restTemplate.exchange(requestPath,HttpMethod.POST,entity,classType);
        T responseDTO;
        try {
            responseDTO = getResponse(responseEntity);
        }catch (Exception e) {
            throw new TransportException(e, TransportErrorCodes.UNKOWN_EXCEPTION);
        }
        return responseDTO;
    }

    @Override
    public <T extends BaseResponseDTO> T executePost(String requestPath, BaseRequestDTO request, Map<String, String> headersMap, Class<T> classType, Protocol protocol, String serviceUrl) throws TransportException {
        validatePost(requestPath,request);
        RestTemplate restTemplate = getCachedRestTemplate(serviceUrl);
        HttpHeaders httpHeaders = getHttpHeaders(headersMap, protocol);
        HttpEntity<BaseDto> entity = new HttpEntity<>(request, httpHeaders);
        ResponseEntity<T> responseEntity = restTemplate.exchange(requestPath,HttpMethod.POST,entity,classType);
        T responseDTO;
        try {
            responseDTO = getResponse(responseEntity);
        }catch (Exception e) {
            throw new TransportException(e, TransportErrorCodes.UNKOWN_EXCEPTION);
        }
        return responseDTO;
    }

    @Override
    public <T extends BaseResponseDTO> T executePost(String requestPath, BaseDto baseRequestDTO, Map<String, String> params, Map<String, String> headersMap, Class<T> classType, Protocol protocol, String serviceUrl) throws TransportException {
        RestTemplate restTemplate = getCachedRestTemplate(serviceUrl);
        HttpHeaders httpHeaders = getHttpHeaders(headersMap, protocol);
        HttpEntity<BaseDto> entity = new HttpEntity<>(baseRequestDTO, httpHeaders);
        ResponseEntity<T> responseEntity = restTemplate.exchange(requestPath,HttpMethod.POST,entity,classType);
        T responseDTO;
        try {
            responseDTO = getResponse(responseEntity);
        }catch (Exception e) {
            throw new TransportException(e, TransportErrorCodes.UNKOWN_EXCEPTION);
        }
        return responseDTO;
    }
    
    @Override
    public <T extends BaseResponseDTO> T executePost(String requestPath, BaseRequestDTO baseRequestDTO, Map<String, String> params, Map<String, String> headersMap, Class<T> classType, Protocol protocol, String serviceUrl) throws TransportException {
        RestTemplate restTemplate = getCachedRestTemplate(serviceUrl);
        HttpHeaders httpHeaders = getHttpHeaders(headersMap, protocol);
        HttpEntity<BaseDto> entity = new HttpEntity<>(baseRequestDTO, httpHeaders);
        ResponseEntity<T> responseEntity = restTemplate.exchange(requestPath,HttpMethod.POST,entity,classType);
        T responseDTO;
        try {
            responseDTO = getResponse(responseEntity);
        }catch (Exception e) {
            throw new TransportException(e, TransportErrorCodes.UNKOWN_EXCEPTION);
        }
        return responseDTO;
    }

    @Override
    public <T extends BaseResponseDTO> T executePost(String requestPath, String body, Map<String, String> params, Map<String, String> headersMap, Class<T> classType, Protocol protocol, String serviceUrl) throws TransportException {
        RestTemplate restTemplate = getCachedRestTemplate(serviceUrl);
        HttpHeaders httpHeaders = getHttpHeaders(headersMap, protocol);
        HttpEntity<String> entity = new HttpEntity<>(body, httpHeaders);
        log.info(entity.toString());
        ResponseEntity<T> responseEntity = restTemplate.exchange(requestPath,HttpMethod.POST,entity,classType);
        T responseDTO;
        try {
            responseDTO = getResponse(responseEntity);
        }catch (Exception e) {
            throw new TransportException(e, TransportErrorCodes.UNKOWN_EXCEPTION);
        }
        return responseDTO;
    }


    @Override
    public <T extends BaseResponseDTO> T executePost(String requestPath, BaseDto baseRequestDTO, Map<String, String> params, Map<String, String> headersMap, Class<T> classType, String serviceUrl) throws TransportException {
        return executePost(requestPath,baseRequestDTO, params,headersMap,classType,Constants.DEFAULT_PROTOCOL,serviceUrl);
    }
    
    @Override
    public <T extends BaseResponseDTO> T executePost(String requestPath, BaseRequestDTO baseRequestDTO, Map<String, String> params, Map<String, String> headersMap, Class<T> classType, String serviceUrl) throws TransportException {
        return executePost(requestPath,baseRequestDTO, params,headersMap,classType,Constants.DEFAULT_PROTOCOL,serviceUrl);
    }

    @Override
    public <T,V> T executePost(String requestPath, V body, Map<String, String> headersMap, Class<T> responseType, String serviceUrl) throws TransportException {
        try {
            RestTemplate restTemplate = getCachedRestTemplate(serviceUrl);
            HttpHeaders headers = getHttpHeaders(headersMap, Protocol.PROTOCOL_JSON);
            HttpEntity<V> entity = new HttpEntity<>(body, headers);
            log.info(entity.toString());
            ResponseEntity<T> responseEntity = restTemplate.exchange(requestPath, HttpMethod.POST, entity, responseType);
            return responseEntity.getBody();
        } catch (Exception e) {
            throw new TransportException(e.getMessage(), e, TransportErrorCodes.UNKOWN_EXCEPTION);
        }
    }

    @Override
    public <T, V> T executeGetWithBody(String requestPath, Map<String,String> params, V body, Map<String, String> headersMap, Class<T> responseType, String serviceUrl) throws TransportException {
        try {
            RestTemplate restTemplate = getCachedRestTemplate(serviceUrl);
            HttpHeaders headers = getHttpHeaders(headersMap, Protocol.PROTOCOL_JSON);
            HttpEntity<V> entity = new HttpEntity<>(body, headers);
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(requestPath);
            if (params != null) {
                params.forEach((key,value) -> builder.queryParam(key,value));
            }
            log.info(entity.toString());
            ResponseEntity<T> responseEntity = restTemplate.exchange(builder.build().encode(Constants.CONTENT_ENCODING_UTF_8).toUri(), HttpMethod.GET, entity, responseType);
            return responseEntity.getBody();
        } catch (Exception e) {
            throw new TransportException(e.getMessage(), e, TransportErrorCodes.UNKOWN_EXCEPTION);
        }
    }

    @Override
    public <T> T executePost(String requestPath, Map<String, String> headersMap, MultiValueMap<String, Object> multiPartFormData, Class<T> responseType, String serviceUrl) throws TransportException {
        try {
            RestTemplate restTemplate = getCachedRestTemplate(serviceUrl);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);
            headers.set(HttpHeaders.ACCEPT, Protocol.PROTOCOL_JSON.getApplicationType());
            if (headersMap != null) {
                headersMap.forEach(headers :: add);
            }
            HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<>(multiPartFormData, headers);
            ResponseEntity<T> responseEntity = restTemplate.exchange(requestPath, HttpMethod.POST, entity, responseType);
            return responseEntity.getBody();
        } catch (Exception e) {
            log.error(String.format("error [%s] in transport service", e.getMessage()),e);
            throw new TransportException(e.getMessage(), e, TransportErrorCodes.UNKOWN_EXCEPTION);
        }
    }


    @Override
    public <T,V> T executePut(String requestPath, V body, Map<String, String> headersMap, Class<T> responseType, String serviceUrl) throws TransportException {
        try {
            RestTemplate restTemplate = getCachedRestTemplate(serviceUrl);
            HttpHeaders headers = getHttpHeaders(headersMap, Protocol.PROTOCOL_JSON);
            if (headersMap != null) {
                headersMap.forEach(headers :: add);
            }
            HttpEntity<V> entity = new HttpEntity<>(body, headers);
            log.info(entity.toString());
            ResponseEntity<T> responseEntity = restTemplate.exchange(requestPath, HttpMethod.PUT, entity, responseType);
            return responseEntity.getBody();
        } catch (Exception e) {
            throw new TransportException(e.getMessage(), e, TransportErrorCodes.UNKOWN_EXCEPTION);
        }
    }

    @Override
    public String executeGet(String requestPath, Map<String, String> params, Map<String, String> headersMap, String serviceUrl) throws TransportException {
        RestTemplate restTemplate = getCachedRestTemplate(serviceUrl);
        HttpHeaders headers = new HttpHeaders();
        if (headersMap != null) {
            headersMap.forEach(headers::add);
        }
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(requestPath);
        if (params != null) {
            params.forEach((key,value) -> builder.queryParam(key,value));
        }
        HttpEntity<String> entity = new HttpEntity<>(headers);
        String responseDTO;
        try {
            ResponseEntity<String> responseEntity = restTemplate.exchange(builder.build().encode(Constants.CONTENT_ENCODING_UTF_8).toUri(), HttpMethod.GET,entity, String.class);
            HttpStatus responseCode = responseEntity.getStatusCode();
            responseDTO = responseEntity.hasBody() ? responseEntity.getBody() : null;
            checkHttpResponse(null, responseCode);
            return responseDTO;
        } catch (UnsupportedEncodingException e) {
            throw new TransportException(e, TransportErrorCodes.SERIALIZATION_EXCEPTION);
        } catch (Exception e) {
            throw new TransportException(e, TransportErrorCodes.UNKOWN_EXCEPTION);
        }
    }

    private void validatePost(String requestPath, BaseDto request) throws TransportException {
        validate(requestPath);
        if (request == null) {
            throw new TransportException("No defined url was found url " + requestPath, TransportErrorCodes.INPUT_MISSING);
        }
    }

    private RestTemplate getCachedRestTemplate(String serviceUrl) {
        String url = serviceUrl != null ? serviceUrl : Constants.DEFAULT;
        //log.info("service url {} serviceToInitialsMap {}", url, serviceToInitialsMap);
        String configInitials = serviceToInitialsMap.get(url);
        RestTemplate restTemplate = httpSenderCache.get(url);
        //log.info("Rest template {} and config initials {}", restTemplate, configInitials);
        if (restTemplate == null) {
            synchronized (TransportServiceImpl.class) {
                if (restTemplate == null) {
                    HttpClientConfig httpClientConfig =  applicationContext.getBean(HttpClientConfig.class);
                    if (serviceUrl != null)
                        httpClientConfig.updateValuesForInitials(configInitials);
                    restTemplate = RestTemplateBuilder.createNewRestTemplate(httpClientConfig);
                    httpSenderCache.put(url, restTemplate);
                }
            }
        }
        return restTemplate;
    }

    private <T extends BaseResponseDTO> T getResponse(ResponseEntity<T> responseEntity) throws TransportException {
        T responseDTO;HttpStatus responseCode = responseEntity.getStatusCode();
        responseDTO = responseEntity.hasBody() ? responseEntity.getBody() : null;
        checkHttpResponse(responseDTO, responseCode);
        return responseDTO;
    }

    private <T extends BaseResponseDTO> void checkHttpResponse(T responseDTO, HttpStatus responseCode) throws TransportException {
        if (!HttpStatus.OK.equals(responseCode)) {
            log.error("Invalid response received. Response code : {} and  data : {} ", responseCode, responseDTO == null ? "null" : responseDTO);
            String errorMessage = "Unknown Error at destination. Response Code : " + responseCode;
            switch (responseCode) {
                case NOT_FOUND:
                    errorMessage = "Requested URL Does Not exist at destination. Response Code : 404";
                    break;
                case INTERNAL_SERVER_ERROR:
                    errorMessage = "Internal Server Error at destination. Response Code : 500";
                    break;
                case FORBIDDEN:
                case UNAUTHORIZED:
                    errorMessage = "Request unauthorized. Response Code : " + responseCode;
                    break;
            }
            throw new TransportException(errorMessage, TransportErrorCodes.REQUEST_TYPE_ERROR_EXCEPTION);
        }
    }

    /**
     *
     *The execute post method.
     * @param requestPath the request path
     * @param headersMap the headers map
     * @param classType the response class type
     * @param protocol the protocol to use
     * @param cacheName the cache name
     * @param <T> the response type
     * @return the response
     * @throws TransportException Exception in execution
     */
    public <T extends BaseResponseDTO> T  executeGet(String requestPath, Map<String, String> params, Map<String, String> headersMap, Class<T> classType, Protocol protocol, String cacheName) throws TransportException {
        validate(requestPath);
        RestTemplate restTemplate = getCachedRestTemplate(cacheName);
        HttpHeaders headers = getHttpHeaders(headersMap, protocol);
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(requestPath);
        if (params != null) {
            params.forEach((key,value) -> builder.queryParam(key,value));
        }
        T responseDTO;
        HttpEntity<String> entity = new HttpEntity<>(headers);
        try {
            ResponseEntity<T> responseEntity = restTemplate.exchange(builder.build().encode(Constants.CONTENT_ENCODING_UTF_8).toUri(), HttpMethod.GET,entity, classType);
            responseDTO = getResponse(responseEntity);
        } catch (UnsupportedEncodingException e) {
            throw new TransportException(e, TransportErrorCodes.SERIALIZATION_EXCEPTION);
        } catch (Exception e) {
            throw new TransportException(e, TransportErrorCodes.UNKOWN_EXCEPTION);
        }
        return responseDTO;
    }
}
