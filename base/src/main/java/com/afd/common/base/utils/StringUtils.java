package com.afd.common.base.utils;

/**
 * Created by faisal on 27/1/18.
 */
public class StringUtils {

   public static boolean isEmpty(String str) {
        if (str == null)
            return true;
        return str.trim().isEmpty();
    }

    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }


    public static String toString(byte[] bytes) {
       return new String(bytes);
    }
}
