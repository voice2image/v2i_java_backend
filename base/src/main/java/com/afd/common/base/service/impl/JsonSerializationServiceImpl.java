package com.afd.common.base.service.impl;

import com.afd.common.base.exception.SerializationException;
import com.afd.common.base.service.SerializationService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

/**
 * Created by jaiprakash on 4/1/17.
 */
@Slf4j
public class JsonSerializationServiceImpl implements SerializationService {

    @Override
    public String doSerialize(Object object) throws SerializationException {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(object);
        } catch (IOException e) {
            log.error("Error serializing json for data " + object, e);
            throw new SerializationException("Error serializing json", e);
        }
    }

    @Override
    public byte[] doSerialize(Class<? extends Object> classType, Object obj) throws SerializationException {
        ObjectMapper mapper = new ObjectMapper();
        byte[] data = null;
        try {
            data = mapper.writeValueAsBytes(obj);
        } catch (IOException e) {
            log.error("Error serializing json for data " + obj, e);
            throw new SerializationException("Error serializing json", e);
        }
        return data;
    }

    @Override
    public Object doDeserialize(byte[] data, Class<? extends Object> classType) throws SerializationException {
        ObjectMapper mapper = new ObjectMapper();
        Object object = null;
        try {
            object = mapper.readValue(data, 0, data.length, classType);
        }  catch (IOException e) {
            log.error("Error deserializing json for data ", e);
            throw new SerializationException("Error deserializing json", e);
        }
        return object;
    }

    @Override
    public Object doDeSerialize(String json, Class<? extends Object> classType) throws SerializationException {
        ObjectMapper mapper = new ObjectMapper();
        Object object = null;
        try {
            object = mapper.readValue(json, classType);
        }  catch (IOException e) {
            log.error("Error serializing json for data " + json, e);
            throw new SerializationException("Error serializing json", e);
        }
        return object;
    }

}
