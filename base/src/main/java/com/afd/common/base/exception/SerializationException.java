package com.afd.common.base.exception;

/**
 * Created by faisal on 27/1/18.
 */
public class SerializationException extends Exception {
    public SerializationException(Exception e) {
        super(e);
    }

    public SerializationException(String message, Exception e) {
        super(message,e);
    }
}
