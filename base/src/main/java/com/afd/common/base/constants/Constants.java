package com.afd.common.base.constants;

/**
 * This class defines the common constants used.
 *
 * Created by jaiprakash on 27/12/16.
 */
public class Constants {

    public static final String CONTENT_ENCODING_UTF_8 = "UTF-8";

    public static final String ACCEPT_ENCODING_HEADER = "Accept-Encoding";

    //Currently only GZIP supported in Accept-Encoding
    public static final String GZIP = "GZIP";
    public static final String DEFAULT = "DEFAULT";
    public static final Protocol DEFAULT_PROTOCOL = Protocol.PROTOCOL_JSON;
}
