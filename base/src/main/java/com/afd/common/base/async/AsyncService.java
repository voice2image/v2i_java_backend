package com.afd.common.base.async;

/**
 * Created by faisal on 25/1/18.
 */
public interface AsyncService {

    boolean doAsync(Runnable r);

    boolean doAsyncNoWait(Runnable r);

}
