package com.afd.common.base.service;

import com.afd.common.base.constants.Protocol;
import com.afd.common.base.exception.TransportException;
import com.afd.common.base.reqresp.BaseDto;
import com.afd.common.base.reqresp.request.BaseRequestDTO;
import com.afd.common.base.reqresp.response.BaseResponseDTO;
import org.springframework.util.MultiValueMap;

import java.util.Map;

/**
 * The transport service
 *
 * Created by faisal on 27/1/18.
 */
public interface TransportService {

    /**
     * Every service is expected to register with a service url and a name that will be used for the configurations.
     * @param serviceUrl the url of the service
     * @param cacheName the config parameters
     */
    void registerService(String serviceUrl, String cacheName);

    /**
     * The execute get method.
     *
     * @param requestPath the path to be used.
     * @param params the parameters to be used
     * @param headersMap the headers map
     * @param classType the response class type
     * @param cacheName the cache name
     * @param <T> the type of response
     * @return the response
     * @throws TransportException Exception in execution
     */
    public <T extends BaseResponseDTO> T executeGet(String requestPath, Map<String, String> params, Map<String, String> headersMap, Class<T> classType, String cacheName)
        throws TransportException;

    /**
     *
     * @param responseType
     * @param requestPath
     * @param params
     * @param headersMap
     * @param serviceUrl
     * @param <T>
     * @return
     * @throws TransportException
     */
    public <T> T executeGet(Class<T> responseType, String requestPath, Map<String, String> params, Map<String, String> headersMap, String serviceUrl) throws TransportException;

    /**
     *The execute post method
     * @param requestPath the request path
     * @param request the request body
     * @param headersMap the headers map
     * @param classType the response class type
     * @param cacheName the cache name
     * @param <T> the response type
     * @return the response
     * @throws TransportException Exception in execution
     */
    public <T extends BaseResponseDTO> T executePost(String requestPath, BaseDto request, Map<String, String> headersMap, Class<T> classType, String cacheName) throws TransportException;

    @Deprecated
    public <T extends BaseResponseDTO> T executePost(String requestPath, BaseRequestDTO request, Map<String, String> headersMap, Class<T> classType, String cacheName) throws TransportException;

    /**
     *
     *The execute post method.
     * @param requestPath the request path
     * @param request the request body
     * @param headersMap the headers map
     * @param classType the response class type
     * @param protocol the protocol to use
     * @param cacheName the cache name
     * @param <T> the response type
     * @return the response
     * @throws TransportException Exception in execution
     */
    public <T extends BaseResponseDTO> T executePost(String requestPath, BaseDto request, Map<String, String> headersMap, Class<T> classType, Protocol protocol, String cacheName) throws TransportException;
    
    @Deprecated
    public <T extends BaseResponseDTO> T executePost(String requestPath, BaseRequestDTO request, Map<String, String> headersMap, Class<T> classType, Protocol protocol, String cacheName) throws TransportException;


    /**
     *
     *The execute post method.
     * @param requestPath the request path
     * @param params the parameters to be used
     * @param headersMap the headers map
     * @param classType the response class type
     * @param protocol the protocol to use
     * @param cacheName the cache name
     * @param <T> the response type
     * @return the response
     * @throws TransportException Exception in execution
     */
    public <T extends BaseResponseDTO> T executePost(String requestPath, BaseDto request, Map<String, String> params, Map<String, String> headersMap, Class<T> classType, Protocol protocol, String cacheName) throws TransportException;
    
    @Deprecated
    public <T extends BaseResponseDTO> T executePost(String requestPath, BaseRequestDTO request, Map<String, String> params, Map<String, String> headersMap, Class<T> classType, Protocol protocol, String cacheName) throws TransportException;

    /**
     *
     *The execute post method.
     * @param requestPath the request path
     * @param body the post body as a string
     * @param params the parameters to be used
     * @param headersMap the headers map
     * @param classType the response class type
     * @param serviceUrl the url that was used for registration. If not specified default values will be used.
     * @param <T> the response type
     * @return the response
     * @throws TransportException Exception in execution
     */
    public <T extends BaseResponseDTO> T executePost(String requestPath, String body, Map<String, String> params, Map<String, String> headersMap, Class<T> classType, Protocol protocol, String serviceUrl) throws TransportException;

    /**
     *
     *The execute post method.
     * @param requestPath the request path
     * @param params the parameters to be used
     * @param headersMap the headers map
     * @param classType the response class type
     * @param cacheName the cache name
     * @param <T> the response type
     * @return the response
     * @throws TransportException Exception in execution
     */
    public <T extends BaseResponseDTO> T executePost(String requestPath, BaseDto request, Map<String, String> params, Map<String, String> headersMap, Class<T> classType, String cacheName) throws TransportException;
    
    @Deprecated
    public <T extends BaseResponseDTO> T executePost(String requestPath, BaseRequestDTO request, Map<String, String> params, Map<String, String> headersMap, Class<T> classType, String cacheName) throws TransportException;

    /**
     *
     * @param requestPath
     * @param body
     * @param headersMap
     * @param responseType
     * @param serviceUrl
     * @param <T> response type
     * @param <V> request body type
     * @return
     * @throws TransportException
     */
    public <T,V> T executePost(String requestPath, V body, Map<String, String> headersMap, Class<T> responseType, String serviceUrl) throws TransportException;


    /**
     * execute get with body
     * @param requestPath
     * @param body
     * @param headersMap
     * @param responseType
     * @param serviceUrl
     * @param <T>
     * @param <V>
     * @return
     * @throws TransportException
     */
    public <T,V> T executeGetWithBody(String requestPath, Map<String, String> params, V body, Map<String, String> headersMap, Class<T> responseType, String serviceUrl) throws TransportException;

    /**
     *
     * @param requestPath
     * @param headersMap
     * @param multiPartFormData
     * @param responseType
     * @param serviceUrl
     * @param <T>
     * @return
     * @throws TransportException
     */
    public <T> T executePost(String requestPath, Map<String, String> headersMap, MultiValueMap<String, Object> multiPartFormData, Class<T> responseType, String serviceUrl) throws TransportException;

    public <T,V> T executePut(String requestPath, V body, Map<String, String> headersMap, Class<T> responseType, String serviceUrl) throws TransportException;

    /**
     * The execute get method.
     *
     * @param requestPath the path to be used.
     * @param params the parameters to be used
     * @param headersMap the headers map
     * @param cacheName the cache name.
     * @return the response
     * @throws TransportException Exception in execution
     */
    public String executeGet(String requestPath, Map<String, String> params, Map<String, String> headersMap, String cacheName) throws TransportException;

    /**
     * The execute get method.
     *
     * @param requestPath the path to be used.
     * @param params the parameters to be used
     * @param headersMap the headers map
     * @param protocol the protocol to use
     * @param cacheName the cache name.
     * @return the response
     * @throws TransportException Exception in execution
     */
    public <T extends BaseResponseDTO> T  executeGet(String requestPath, Map<String, String> params, Map<String, String> headersMap, Class<T> classType, Protocol protocol, String cacheName) throws TransportException;

}
