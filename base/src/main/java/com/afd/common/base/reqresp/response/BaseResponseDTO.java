package com.afd.common.base.reqresp.response;

import com.afd.common.base.exception.ValidationError;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.io.Serializable;
import java.util.List;

/**
 * Created by faisal on 25/1/18.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
public class BaseResponseDTO implements Serializable {

    private static final long serialVersionUID = 1012981356588067789L;

    private int code;

    private String message;

    private List<ValidationError> validationErrors;
}
