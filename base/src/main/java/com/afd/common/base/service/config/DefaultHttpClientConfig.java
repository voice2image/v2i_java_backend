package com.afd.common.base.service.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;

/**
 * The default configuration values for HTTP Client
 * Created by faisal on 27/1/18.
 */
@Getter
public class DefaultHttpClientConfig {

    @Value("${default.max.connections:100}")
    private int maxConnections;
}
