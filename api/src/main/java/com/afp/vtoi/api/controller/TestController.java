package com.afp.vtoi.api.controller;

import com.afd.common.base.exception.ProcessingException;
import com.afd.vtoi.common.request.DeviceTypeEnum;
import com.afp.vtoi.externalservice.service.TextToImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("test")
public class TestController {

    @Autowired
    private TextToImageService textToImageService;

    @RequestMapping("texttoimageurl")
    public ResponseEntity<byte[]> getImageUrlFromText(@RequestParam(value = "q") String query,
                                          @RequestParam(value = "ln", required = false) String language,
                              @RequestHeader(value = "d") DeviceTypeEnum deviceTypeEnum) throws ProcessingException {
        System.out.println("q=" + query + " ln=" + language + " deviceType=" + deviceTypeEnum);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_PNG);
        headers.setCacheControl(CacheControl.noCache().getHeaderValue());
        byte[] media = textToImageService.getTextToImage(query);
        ResponseEntity<byte[]> responseEntity = new ResponseEntity<>(media, headers, HttpStatus.OK);
        return responseEntity;
    }
}