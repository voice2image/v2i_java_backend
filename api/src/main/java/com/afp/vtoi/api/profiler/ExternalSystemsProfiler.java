package com.afp.vtoi.api.profiler;


import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * Created by faisal on 26/1/18.
 */
@Aspect
@Component
@Slf4j
public class ExternalSystemsProfiler {

    @Pointcut("execution(* com.vyom.external.service.*.*(..))")
    public void executionTimeProfiler() { }

    @Around("executionTimeProfiler()")
    public Object profile(ProceedingJoinPoint pjp) throws Throwable {
        long start = System.currentTimeMillis();
        log.info("Entering in Method :  " + pjp.getSignature().getName() + " args " + Arrays.toString(pjp.getArgs()));
        Object output = pjp.proceed();
        long elapsedTime = System.currentTimeMillis() - start;
        log.info("Response " + output);
        log.info("Method " + pjp.getSignature().getName() + " execution time: " + elapsedTime + " milliseconds.");
        return output;
    }
}
