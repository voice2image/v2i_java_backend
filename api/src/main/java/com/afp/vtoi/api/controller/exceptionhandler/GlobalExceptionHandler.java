package com.afp.vtoi.api.controller.exceptionhandler;

import com.afd.common.base.exception.ProcessingException;
import com.afd.common.base.exception.TransportException;
import com.afd.common.base.exception.ValidationError;
import com.afd.common.base.exception.ValidationException;
import com.afd.common.base.reqresp.response.BaseResponseDTO;
import com.afd.common.base.reqresp.response.ResponseCodeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by faisal on 27/1/17.
 */
@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

  @Value("${spring.profiles.active:local}")
  private String profile;

  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  @ExceptionHandler(value = Exception.class)
  @ResponseBody
  public BaseResponseDTO handleBaseException(Exception exception) {
    log.error("Error happened", exception);
    BaseResponseDTO baseResponseDTO = new BaseResponseDTO();
    baseResponseDTO.setCode(ResponseCodeEnum.FAILURE.getCode());
    if (profile.equalsIgnoreCase("local") || profile.equalsIgnoreCase("dev"))
      baseResponseDTO.setMessage(exception.getMessage());
    else
      baseResponseDTO.setMessage(ResponseCodeEnum.FAILURE.name());
    return baseResponseDTO;
  }

  @ResponseStatus(HttpStatus.OK)
  @ExceptionHandler(value = ValidationException.class)
  @ResponseBody
  public BaseResponseDTO handleBaseException(ValidationException exception,
      HttpServletRequest request) {
    log.error("Validation error happened {}", exception.getValidationErrorList().toString());
    BaseResponseDTO baseResponseDTO = new BaseResponseDTO();
    baseResponseDTO.setCode(ResponseCodeEnum.VALIDATION_ERROR.getCode());
    String message = "";
    baseResponseDTO.setMessage(message);
    baseResponseDTO.setValidationErrors(exception.getValidationErrorList());
    return baseResponseDTO;
  }

  @ResponseStatus(HttpStatus.OK)
  @ExceptionHandler(value = {TransportException.class})
  @ResponseBody
  public BaseResponseDTO handleBaseException(TransportException exception) {
    log.error("TransportException error happened", exception);
    BaseResponseDTO baseResponseDTO = new BaseResponseDTO();
    baseResponseDTO.setCode(ResponseCodeEnum.COMMUNICATION_BREAKDOWN.getCode());
    baseResponseDTO.setMessage(ResponseCodeEnum.COMMUNICATION_BREAKDOWN.name());
    return baseResponseDTO;
  }


  @ResponseStatus(HttpStatus.OK)
  @ExceptionHandler(value = {ProcessingException.class})
  @ResponseBody
  public BaseResponseDTO handleProcessingExceptionException(ProcessingException exception) {
    log.error("ProcessingException error happened", exception);
    BaseResponseDTO baseResponseDTO = new BaseResponseDTO();
    //TODO language changes and better error
    baseResponseDTO.setCode(ResponseCodeEnum.COMMUNICATION_BREAKDOWN.getCode());
    baseResponseDTO.setMessage(ResponseCodeEnum.COMMUNICATION_BREAKDOWN.name());
    return baseResponseDTO;
  }

  @ResponseStatus(HttpStatus.OK)
  @ExceptionHandler(value = {MethodArgumentNotValidException.class, IllegalArgumentException.class})
  @ResponseBody
  public BaseResponseDTO methodArgumentNotValidExceptionHandler(MethodArgumentNotValidException e,
      HttpServletRequest request) {
    log.error("MethodArgumentNotValidException: " + e.getMessage());

    List<ValidationError> errors = new ArrayList<>();

    for (ObjectError error : e.getBindingResult().getGlobalErrors()) {
//      String message = errorStringsService.getStringFor(error.getDefaultMessage(), userLanguage);
//      errors.add(new ValidationError(ResponseDtoCodeEnum.VALIDATION_ERROR.getCode(), message));
    }

    for (FieldError fieldError : e.getBindingResult().getFieldErrors()) {
//      String message =
//          errorStringsService.getStringFor(fieldError.getDefaultMessage(), userLanguage);
//      errors.add(new ValidationError(ResponseDtoCodeEnum.VALIDATION_ERROR.getCode(), message));
    }

    BaseResponseDTO responseDto = null;
//        new BaseResponseDTO(ResponseDtoCodeEnum.VALIDATION_ERROR.getCode(),
//            CollectionUtils.isEmpty(errors)
//                ? ResponseDtoCodeEnum.VALIDATION_ERROR.name()
//                : errors.get(0).getMessage(),
//            errors);
    return responseDto;
  }
  
//  @ResponseStatus(HttpStatus.OK)
//  @ExceptionHandler(value = NotificationException.class)
//  @ResponseBody
//  public BaseResponseDTO handlePendingPaymentException(NotificationException pendingPaymentException,
//      HttpServletRequest request) {
//    log.error("Error happened " + pendingPaymentException.getMessage());
//   
//    BaseResponseWithNotificationDto response = pendingPaymentException.getBaseResponseWithNotificationDto();
//    
//    if(!response.getShouldIncludeOriginalResponse()) {
//      BaseResponseWithNotificationDto newResponse = new BaseResponseWithNotificationDto();
//      newResponse.setMessage(response.getMessage());
//      newResponse.setNotification(response.getNotification());
//      newResponse.setNotificationType(response.getNotificationType());
//      response = newResponse;
//    }
//    
//    return response;
//  }

/*  @ExceptionHandler(value = VersionException.class)
  @ResponseBody
  public VersionResponseDto handleVersionException(VersionException versionException,
                                                   HttpServletRequest request, HttpServletResponse response) {
    log.error("Version error happened" + versionException.getApplicationVersion());
    String userLanguage = request.getHeader(HeaderConstants.LANGUAGE);
    if (userLanguage == null) {
      userLanguage = CommonConstants.DEFAULT_LANGUAGE;
    }
    Configuration messageConfiguration, titleConfiguration;
    VersionResponseDto versionResponseDto = new VersionResponseDto();
    String appVersion = versionException.getApplicationVersion();
    String oldVersionConcatenated = configurationService.getStringProperty(OLD_APPLICATION_VERSION_SUPPORTED,"");
    List<String> oldVersionList = Arrays.asList(oldVersionConcatenated.split(","));
    if(!oldVersionList.contains(appVersion)){
      messageConfiguration = configurationService.getConfigurationForKey(VERSION_NOT_SUPPORTED_MESSAGE);
      titleConfiguration = configurationService.getConfigurationForKey(VERSION_NOT_SUPPORTED_TITLE);
      SpecialNotificationObject specialNotificationObject = getSpecialNotificationForVersion(
          titleConfiguration, messageConfiguration, userLanguage);
      versionResponseDto.setSpecialNotificationObject(specialNotificationObject);
      versionResponseDto.setCode(ResponseDtoCodeEnum.VERSION_OLD.getCode());
      response.setStatus(HttpServletResponse.SC_GONE);
    }
    return versionResponseDto;
  }*/

/*  @ResponseStatus(HttpStatus.OK)
  @ExceptionHandler(value = ThrottledException.class)
  @ResponseBody
  public BaseResponseDTO handleThrottledException(ThrottledException throttledException,
      HttpServletRequest request) {
    log.error("Throttling happened " + throttledException);
    BaseResponseDTO baseResponseDTO = new BaseResponseDTO();
    return baseResponseDTO;
  }*/
}
