package com.afd.vtoi.common.response;

import com.afd.common.base.reqresp.response.BaseResponseDTO;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TextToImageResponseDto extends BaseResponseDTO {
    private String url;
}
