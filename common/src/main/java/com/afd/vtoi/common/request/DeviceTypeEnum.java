package com.afd.vtoi.common.request;

import lombok.Getter;

@Getter
public enum DeviceTypeEnum {
    A(0, "Android"), I(1, "Iphone");
    private int code;
    private String description;

    DeviceTypeEnum(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public static DeviceTypeEnum findById(Integer id) {
        if(id != null) {
            for (DeviceTypeEnum deviceTypeEnum : DeviceTypeEnum.values()) {
                if (deviceTypeEnum.getCode() == id) {
                    return deviceTypeEnum;
                }
            }
        }
        return null;
    }
}
