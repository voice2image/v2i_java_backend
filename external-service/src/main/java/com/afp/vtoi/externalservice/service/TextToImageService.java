package com.afp.vtoi.externalservice.service;

import com.afd.common.base.exception.ProcessingException;

public interface TextToImageService {
    void registerWebServiceUrl(String url);
    byte[] getTextToImage(String text) throws ProcessingException;
}
