package com.afp.vtoi.externalservice.init;

import com.afp.vtoi.externalservice.config.TextToImageServiceConfig;
import com.afp.vtoi.externalservice.service.TextToImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class StartupService {

    @Autowired
    private TextToImageService textToImageService;

    @Autowired
    private TextToImageServiceConfig textToImageServiceConfig;

    @PostConstruct
    public void startupService() {
        textToImageService.registerWebServiceUrl(textToImageServiceConfig.getTexttoimageServiceUrl());
    }
}
