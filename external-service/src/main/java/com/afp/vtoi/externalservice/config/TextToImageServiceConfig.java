package com.afp.vtoi.externalservice.config;

import lombok.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * Created by faisal on 29/1/18.
 */
@Configuration
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TextToImageServiceConfig {

    @Value("${texttoimage.service.url:http://ec2-52-213-176-45.eu-west-1.compute.amazonaws.com:8089}")
    public String texttoimageServiceUrl;

}