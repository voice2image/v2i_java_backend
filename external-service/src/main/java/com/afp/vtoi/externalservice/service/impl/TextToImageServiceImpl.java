package com.afp.vtoi.externalservice.service.impl;


import com.afd.common.base.exception.ProcessingException;
import com.afd.common.base.service.TransportService;
import com.afp.vtoi.externalservice.service.TextToImageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class TextToImageServiceImpl implements TextToImageService {

    private static final String BASE_URL = "";
    private static final String INITIALS = "";

    private String webUrl;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public void registerWebServiceUrl(String url) {
        webUrl = url + BASE_URL;
//        transportService.registerService(webUrl, INITIALS);
    }

    @Override
    public byte[] getTextToImage(String text) throws ProcessingException {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM));
        log.error("Url {}", webUrl);
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        ResponseEntity<byte[]> response = restTemplate.exchange(
                webUrl + "/text/" + text,
                HttpMethod.GET, entity, byte[].class);
        if (response.getStatusCode() == HttpStatus.OK) {
            return response.getBody();
        } else {
            throw new ProcessingException("Please try again");
        }
    }
}
